class AddSecondaryInterviewerToN2Ads < ActiveRecord::Migration
  def change
    add_column :n2_ads, :secondary_interviewer, :string
  end
end
