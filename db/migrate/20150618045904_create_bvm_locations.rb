class CreateBvmLocations < ActiveRecord::Migration
  def change
    create_table :bvm_locations do |t|
      t.float :lat
      t.float :lng
      t.string :job_title
      t.string :company
      t.string :state
      t.string :location

      t.timestamps
    end
  end
end
