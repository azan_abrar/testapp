class CreateN2Ads < ActiveRecord::Migration
  def change
    create_table :n2_ads do |t|
      t.float :lat
      t.float :lng
      t.string :city
      t.string :state
      t.string :primary_interviewer

      t.timestamps
    end
  end
end
