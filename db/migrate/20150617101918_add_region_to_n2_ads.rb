class AddRegionToN2Ads < ActiveRecord::Migration
  def change
    add_column :n2_ads, :region, :string
  end
end
