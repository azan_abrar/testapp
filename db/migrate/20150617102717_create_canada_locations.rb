class CreateCanadaLocations < ActiveRecord::Migration
  def change
    create_table :canada_locations do |t|
      t.float :lat
      t.float :lng
      t.string :job_title
      t.string :company
      t.string :city
      t.string :state

      t.timestamps
    end
  end
end
