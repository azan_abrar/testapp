class CreateStoredUrls < ActiveRecord::Migration
  def change
    create_table :stored_urls do |t|
      t.string :link

      t.timestamps
    end
  end
end
