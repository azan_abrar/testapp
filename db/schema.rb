# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150618053409) do

  create_table "bvm_locations", force: true do |t|
    t.float    "lat",        limit: 24
    t.float    "lng",        limit: 24
    t.string   "job_title"
    t.string   "company"
    t.string   "state"
    t.string   "location"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "canada_locations", force: true do |t|
    t.float    "lat",        limit: 24
    t.float    "lng",        limit: 24
    t.string   "job_title"
    t.string   "company"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "n2_ads", force: true do |t|
    t.float    "lat",                   limit: 24
    t.float    "lng",                   limit: 24
    t.string   "city"
    t.string   "state"
    t.string   "primary_interviewer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "region"
    t.string   "secondary_interviewer"
  end

  create_table "stored_urls", force: true do |t|
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
