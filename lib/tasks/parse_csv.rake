namespace :parse_csv do

	task parse: :environment do
		require 'csv'

		puts "- - - - - - - - -"
		puts "N2Ads TASK STARTED"
		puts "- - - - - - - - -"
		CSV.foreach("lib/csv/n2ads.csv") do |row|
			#Put false when to skip and true when not to skip
			if false
				n2ads = N2Ads.where("city = ? AND state = ? AND region = ?", row[2], row[1], row[0]).last
				n2ads = N2Ads.new(city: row[2], state: row[1], region: row[0]) if n2ads.blank?
				n2ads.primary_interviewer = row[3]
				n2ads.secondary_interviewer = row[4]
				if n2ads.lat.blank? && n2ads.lng.blank?
					coordinates = Geocoder.coordinates("#{n2ads.region}, #{n2ads.city}, #{n2ads.state}") rescue nil
					if coordinates.present?
						n2ads.lat 	= coordinates[0]
						n2ads.lng 	= coordinates[1]
						puts "#{n2ads.region}, #{n2ads.city}, #{n2ads.state}"
					end
				end	
				n2ads.save
			end
		end

		puts "- - - - - - - - -"
		puts "CanadaLocation TASK STARTED"
		puts "- - - - - - - - -"
		CSV.foreach("lib/csv/canada_locations.csv") do |row|
			#Put false when to skip and true when not to skip
			if false
				canada_location = CanadaLocation.where("city = ? AND state = ?", row[2], row[3]).last
				canada_location = CanadaLocation.new(city: row[2], state: row[3]) if canada_location.blank?
				canada_location.job_title 	= row[0]
				canada_location.company 	= row[1]
				if canada_location.lat.blank? && canada_location.lng.blank?
					coordinates = Geocoder.coordinates("#{canada_location.city}, #{canada_location.state}") rescue nil
					if coordinates.present?
						canada_location.lat 	= coordinates[0]
						canada_location.lng 	= coordinates[1]
						puts "#{canada_location.city}, #{canada_location.state}"
					end
				end
				canada_location.save
			end
		end

		puts "- - - - - - - - -"
		puts "BvmLocation TASK STARTED"
		puts "- - - - - - - - -"
		CSV.foreach("lib/csv/bvm_locations.csv") do |row|	
			#Put false when to skip and true when not to skip
			if true		
				bvm_location = BvmLocation.where("location = ?", row[3]).last
				bvm_location = BvmLocation.new(location: row[3]) if bvm_location.blank?
				bvm_location.job_title 		= row[0]
				bvm_location.company 		= row[1]
				bvm_location.state 			= row[2]
				if bvm_location.lat.blank? && bvm_location.lng.blank?
					coordinates = Geocoder.coordinates("#{bvm_location.location}") rescue nil
					if coordinates.present?
						bvm_location.lat 	= coordinates[0]
						bvm_location.lng 	= coordinates[1]
						puts "#{bvm_location.location}"
					end
				end
				bvm_location.save
			end
		end
	end
end