class StoredUrl < ActiveRecord::Base
	validates :link, presence: true, uniqueness: { case_sensitive: false }, format: {
	with: /\A(?:[a-zA-Z]+\.){0,1}(?:[a-zA-Z0-9][a-zA-Z0-9-]+){1}(?:\.[a-zA-Z]{2,6})+\z/}
end
