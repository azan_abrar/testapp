class StoredUrlsController < ApplicationController
  def create
    added = 0
    if params[:urls].present?
      params[:urls].each do |url|
        store_url = StoredUrl.new(link: url)
        added += 1 if store_url.save
      end
      message = "Added Urls: #{added}"
    else
      message = "No Url Found"
    end
    render json: {message: message}
  end
end
