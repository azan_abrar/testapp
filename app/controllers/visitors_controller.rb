class VisitorsController < ApplicationController
	def index
		@urls = StoredUrl.order("updated_at DESC")
	end

	def map
		require "json"

		#-------------->N2ADS
		n2ad_limit = 301
		n2ad_offset = (params[:page].blank? ? 0 : (params[:page].to_i - 1) * n2ad_limit)
		n2ads = N2Ads.where('lat <> ? AND lat IS NOT NULL AND lng <> ? AND lng IS NOT NULL', '', '').limit(n2ad_limit).offset(n2ad_offset)
		collection_n2ads = n2ads.map do |n2ad|
		   { lat: n2ad.lat, lng: n2ad.lng, city: n2ad.city.to_s, infowindow: "Primary Interviewer: <strong>#{n2ad.primary_interviewer.to_s}</strong><br>Secondary Interviewer: <strong>#{n2ad.secondary_interviewer.present? ? n2ad.secondary_interviewer : 'N/A'}</strong><br>Region: <strong>#{n2ad.region}</strong>",  icon: 'marker-red.png' }
		end

		#-------------->Canada Locations
		canada_locations = CanadaLocation.select(:lat, :lng, :city, :state).where('lat <> ? AND lat IS NOT NULL AND lng <> ? AND lng IS NOT NULL', '', '')
		collection_canada = canada_locations.map do |canada_location|
		   { lat: canada_location.lat, lng: canada_location.lng, city: canada_location.city.to_s, infowindow: "Location: <strong>#{canada_location.city}, #{canada_location.state}</strong>",  icon: 'marker-blue.png'}
		end

		#-------------->Bvm Location
		bvm_limit = 500
		bvm_offset = (params[:page].blank? ? 0 : (params[:page].to_i - 1) * bvm_limit)
		bvm_locations = BvmLocation.select(:lat, :lng, :location).where('lat <> ? AND lat IS NOT NULL AND lng <> ? AND lng IS NOT NULL', '', '').limit(bvm_limit).offset(bvm_offset)
		collection_bvm = bvm_locations.map do |bvm_location|
		   { lat: bvm_location.lat, lng: bvm_location.lng, infowindow: "Location: <strong>#{bvm_location.location}</strong>",  icon: 'marker-blue.png'}
		end
		
		merged_hash = collection_n2ads + collection_canada + collection_bvm
		@final_hash = merged_hash.to_json
	end
end