// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require intl-phone/js/intlTelInput
//= require intl-phone/libphonenumber/build/utils.js
//= require gmaps/google
//= require underscore
//= require app
//= require_tree .

function checkOrAddUrl(field, box, message){
	urls = urls||[];
	var url_pattern = /^(http(s?):\/\/)?(?:[a-zA-Z]+\.){0,1}(?:[a-zA-Z0-9][a-zA-Z0-9-]+){1}(?:\.[a-zA-Z]{2,6})+(\/.*)?$/;
	if(field.val().match(url_pattern)){
		url = field.val();
		url = url.replace(/^(http(s?):\/\/)/, '').replace(/\/.*$/, '').toLowerCase();	
		if($.inArray(url, urls)>=0){
			message.html('Already Added');
		}else{
			urls.push(url);
			box.prepend('<li pending>'+url+'</li>');
			field.val('');
		}
	}else{
		message.html('Invalid URL');
	}
}

function checkAndSyncUrls(identifier, url){
	var sync_urls = [];
	$(identifier+' li[pending]').each(function(){
		$(this).removeAttr('pending');
		sync_urls.push($(this).text());
	});
	if(sync_urls.length > 0){
		$.ajax({
			url: url,
			type: "POST",
			data: {urls: sync_urls}
		});
	}
}