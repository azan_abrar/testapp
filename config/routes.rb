Rails.application.routes.draw do
  resources :stored_urls, only: [:create]

  root to: 'visitors#index'

  post 'call' => 'twilio#call'
  post 'connect' => 'twilio#connect'
  get 'twilio' => 'twilio#index'
  post 'voice' => 'twilio#voice'
  get 'generate_call' => 'twilio#generate_call'
  get 'map' => 'visitors#map'
end
